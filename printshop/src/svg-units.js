/*
 * Iterates through svg element, and all child nodes, and converts all length attributes to
 * specified units.
 *
 * Written by Ben McDonald <mcdonald.ben@gmail.com>
 */

/** @type {function():!Object} */
function SVGUnitsDefinition() {
    'use strict';

    /** @type {!Object.<string, number>} */
    var stringToSvgUnitMap = {
        '': 5,
        '%': 2,
        'em': 3,
        'ex': 4,
        'px': 5,
        'cm': 6,
        'mm': 7,
        'in': 8,
        'pt': 9,
        'pc': 10
    };

    /** @type {!Object.<number, string>} */
    var svgUnitToStringMap = {
        1: 'px',
        2: '%',
        3: 'em',
        4: 'ex',
        5: 'px',
        6: 'cm',
        7: 'mm',
        8: 'in',
        9: 'pt',
        10: 'pc',
    };

    /** @type {function(!Element)} */
    function getUnitType(elem) {
        var svgLengthAttributes = Array.from(elem.attributes)
            .filter(function (attribute) {
                return SVGAnimatedLength.prototype.isPrototypeOf(elem[attribute.name]);
            })
            .filter(function (attribute) {
                return elem[attribute.name].baseVal.unitType !== SVGLength.SVG_LENGTHTYPE_PERCENTAGE;
            });

        if (svgLengthAttributes.length) {
            /** @type {number} */
            var unitCode = elem[svgLengthAttributes[0].name].baseVal.unitType
            return svgUnitToStringMap[unitCode];
        } else {
            return 'px';
        }
    }

    /** @type {function(!Element, string)} */
    function updateElementUnit(elem, newUnit) {
        Array.from(elem.attributes)
            .filter(function (attribute) {
                return SVGAnimatedLength.prototype.isPrototypeOf(elem[attribute.name])
			&& elem[attribute.name].baseVal.unitType !== SVGLength.SVG_LENGTHTYPE_PERCENTAGE
			&& elem[attribute.name].baseVal.value;
            })
            .forEach(function (attribute) {
                var ownerSVGElement = elem.ownerSVGElement || document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                var svgLength = ownerSVGElement.createSVGLength();
                svgLength.value = elem[attribute.name].baseVal.value;

                svgLength.convertToSpecifiedUnits(stringToSvgUnitMap[newUnit])
                attribute.value = svgLength.valueInSpecifiedUnits + newUnit;
            });
    }

    /**
    * Iterates through svgDoc element, and all child nodes, and converts all length attributes to
    * specified units.
    *
    * @type {function(!Element, string)}
    */
    function convertSVGToNewUnit(svgDoc, newUnit) {
        Array.from(svgDoc.querySelectorAll('*')).concat([svgDoc]).forEach(function (e) {
            updateElementUnit(e, newUnit);
        });
    }

    return {
        'getUnitType': getUnitType,
        'updateElementUnit': updateElementUnit,
        'convertSVGToNewUnit': convertSVGToNewUnit
    }
}

/** @type {!Object} */
var SVGUnits = new SVGUnitsDefinition();
