<?php 
define("FILE","File");// File
define("NEWDOCUMENT","New Document");//New Document
define("LAYERS","Layers");//Layers
define("NEWLAYER","New Layer");//New Layer
define("DELETELAYER","Delete Layer");//Delete Layer
define("MOREOPTIONS","More Options");//More Options
define("ABOUTTHISEDITOR","About this Editor...");//About this Editor...
define("OPENSVG","Open SVG...");//Open SVG...
define("SAVEIMAGETOSERVER","Save Image to Server");//Save Image to Server
define("CANVAS","Canvas");//Canvas
define("WIDTH","Width");//Width
define("HEIGHT","Height");//Height
define("COLOR","Color");//Color
define("CUSTOM","Custom");//Custom
define("SIZES","Sizes");//Sizes
define("ORDER","Order");//Order
define("SAVE","Save");//Save
define("SAVEORDER","Save & order");//Save & order
define("HELP","Help");//Help
define("DESIGNIDEAS","Design Ideas");//Design Ideas
define("IMAGELIBRARY","Image Library");//Image Library
define("QUICKUPLOAD","Quick Upload");//Quick Upload
define("IMAGEANDGALLERY","Image & Gallery");//Image & Gallery
define("EDIT","Edit");//Edit
define("UNDO","Undo");//Undo
define("REDO","Redo");//Redo
define("CUT","Cut");//Cut
define("COPY","Copy");//Copy
define("PASTE","Paste");//Paste
define("DUPLICATE","Duplicate");//Duplicate
define("DELETE","Delete");//Delete
define("_OBJECT","Object");//Object
define("BRINGTOFRONT","Bring to Front");//Bring to Front
define("BRINGFORWARD","Bring Forward");//Bring Forward
define("SENDBACKWARD","Send Backward");//Send Backward
define("SENDTOBACK","Send to Back");//Send to Back
define("GROUPELEMENTS","Group Elements");//Group Elements
define("UNGROUPELEMENTS","Ungroup Elements");//Ungroup Elements
define("CONVERTTOPATH","Convert to Path");//Convert to Path
define("REORIENTPATH","Reorient path");//Reorient path
define("VIEW","View");//View
define("VIEWRULERS","View Rulers");//View Rulers
define("VIEWWIREFRAME","View Wireframe");//View Wireframe
define("SNAPTOGRID","Snap to Grid");//Snap to Grid
define("SOURCE","Source...");//Source...
define("RECTANGLE","Rectangle");//Rectangle
define("PATH","Path");//Path
define("IMAGE","Image");//Image
define("CIRCLE","Circle");//Circle
define("CENTERX","Center X");//Center X
define("CENTERY","Center Y");//Center Y
define("RADIUS","Radius");//Radius
define("ELLIPSE","Ellipse");//Ellipse
define("RADIUSX","Radius X");//Radius X
define("RADIUSY","Radius Y");//Radius Y
define("LINE","Line");//Line
define("STARTX","Start X");//Start X
define("STARTY","Start Y");//Start Y
define("ENDX","End X");//End X
define("ENDY","End Y");//End Y
define("TEXT","Text");//Text
define("FONT","Font");//Font
define("FONTSIZE","Font Size");//Font Size
define("GROUP","Group");//Group
define("EDITPATH","Edit Path");//Edit Path
define("SEGMENTTYPE","Segment Type");//Segment Type
define("STRAIGHT","Straight");//Straight
define("CURVE","Curve");//Curve
define("ADDNOTE","Add Node");//Add Node
define("DELETENODE","Delete Node");//Delete Node
define("OPENPATH","Open Path");//Open Path
define("ROTATION","Rotation");//Rotation
define("OPACITY","Opacity");//Opacity
define("BLUR","Blur");//Blur
define("ROUNDNESS","Roundness");//Roundness
define("ALIGN","Align");//Align
define("MULTIPLEELEMENTS","Multiple Elements");//Multiple Elements
define("STROKE","Stroke");//Stroke
define("STROKEWIDTH","Stroke Width");//Stroke Width
define("UNGROUP","Ungroup");//Ungroup
define("IMPORTIMAGE","Import Image...");//Import Image...
define();//
define();//
define();//
define();//
define();//
define();//
define();//



$login_check = '<p>To save your images you have to create your account or login if account already exist...<a class="modalCloseImg simplemodal-close" title="Close"></a></p>
							<div style="float:left"><a href="'.$url.'index.php/customer/account/login">Login</a></div><div style="float:left;margin-left:100px;"><a href="'. $url.'index.php/customer/account/create">Register</a></div><div style="float:left;margin-left:100px;"><a href="'. $url.'" class="simplemodal-close">Continue as Guest</a></div>';
$about_text = '<p><b>PrintShop Designer V.1.0<br/>Copyright &copy; 2014 Clixmedia GmbH, www.clixmedia.ch</b><a class="modalCloseImg simplemodal-close" title="Close"></a></p>
							
							<p>This online Designer is a fork from svg-edit open source editor. Special thanks goes to Shweta Chaurasia for programming and Marc MacKay for initial designer work.</p>
							<p><i>Method Draw is Copyright &copy;</i><br/><i>Mark MacKay</i></p>
							

							<p><i>SVG Edit is Copyright (c)</br>
							Narendra Sisodiya</br>
							Pavol Rusnak </br>
							Jeff Schiller </br>
							Vidar Hokstad</br>
							Alexis Deveria</i></p>';
							$help1 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help2 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help3 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help4 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help5 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help6 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';

?>