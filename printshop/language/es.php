<?php 
define("FILE","expediente");// File
define("NEWDOCUMENT","Nuevo documento");//New Document
define("LAYERS","Capas");//Layers
define("NEWLAYER","Nueva capa");//New Layer
define("DELETELAYER","Eliminar capa");//Delete Layer
define("MOREOPTIONS","Más opciones");//More Options
define("ABOUTTHISEDITOR","Acerca de este editor ...");//About this Editor...
define("OPENSVG","Abrir SVG ...");//Open SVG...
define("SAVEIMAGETOSERVER","Guardar imagen al servidor");//Save Image to Server
define("CANVAS","lienzo");//Canvas
define("WIDTH","ancho");//Width
define("HEIGHT","altura");//Height
define("COLOR","color");//Color
define("CUSTOM","costumbre");//Custom
define("SIZES","tamaños");//Sizes
define("ORDER","orden");//Order
define("SAVE","Save");//Save
define("SAVEORDER","Save & order");//Save & order
define("HELP","Help");//Help
define("DESIGNIDEAS","Design Ideas");//Design Ideas
define("IMAGELIBRARY","Image Library");//Image Library
define("QUICKUPLOAD","Quick Upload");//Quick Upload
define("IMAGEANDGALLERY","Image & Gallery");//Image & Gallery
define("EDIT","editar");//Edit
define("UNDO","deshacer");//Undo
define("REDO","rehacer");//Redo
define("CUT","cortada");//Cut
define("COPY","copia");//Copy
define("PASTE","pasta");//Paste
define("DUPLICATE","duplicar");//Duplicate
define("DELETE","borrar");//Delete
define("_OBJECT","objeto");//Object
define("BRINGTOFRONT","Traer al frente");//Bring to Front
define("BRINGFORWARD","adelantar");//Bring Forward
define("SENDBACKWARD","Enviar atrás");//Send Backward
define("SENDTOBACK","Enviar al fondo");//Send to Back
define("GROUPELEMENTS","elementos del Grupo");//Group Elements
define("UNGROUPELEMENTS","Desagrupar elementos");//Ungroup Elements
define("CONVERTTOPATH","Convertir en trazado");//Convert to Path
define("REORIENTPATH","reorientar camino");//Reorient path
define("VIEW","vista");//View
define("VIEWRULERS","ver gobernantes");//View Rulers
define("VIEWWIREFRAME","Ver Wireframe");//View Wireframe
define("SNAPTOGRID","Ajustar a la cuadrícula");//Snap to Grid
define("SOURCE","Fuente ...");//Source...
define("RECTANGLE","rectángulo");//Rectangle
define("PATH","camino");//Path
define("IMAGE","imagen");//Image
define("CIRCLE","círculo");//Circle
define("CENTERX","Centro X");//Center X
define("CENTERY","Centro Y");//Center Y
define("RADIUS","radio");//Radius
define("ELLIPSE","elipse");//Ellipse
define("RADIUSX","Radio X");//Radius X
define("RADIUSY","Radio Y");//Radius Y
define("LINE","línea");//Line
define("STARTX","Comenzar X");//Start X
define("STARTY","Comenzar Y");//Start Y
define("ENDX","Fin X");//End X
define("ENDY","Fin Y");//End Y
define("TEXT","texto");//Text
define("FONT","fuente");//Font
define("FONTSIZE","Tamaño de la Fuente");//Font Size
define("GROUP","grupo");//Group
define("EDITPATH","Editar trayecto");//Edit Path
define("SEGMENTTYPE","Tipo de segmento");//Segment Type
define("STRAIGHT","recto");//Straight
define("CURVE","curva");//Curve
define("ADDNOTE","Agregar nodo");//Add Node
define("DELETENODE","Eliminar nodo");//Delete Node
define("OPENPATH","Camino Abierto");//Open Path
define("ROTATION","rotación");//Rotation
define("OPACITY","opacidad");//Opacity
define("BLUR","mancha");//Blur
define("ROUNDNESS","redondez");//Roundness
define("ALIGN","alinear");//Align
define("MULTIPLEELEMENTS","Elementos Múltiples");//Multiple Elements
define("STROKE","carrera");//Stroke
define("STROKEWIDTH","Stroke Ancho");//Stroke Width
define("UNGROUP","Desagrupar");//Ungroup
define("IMPORTIMAGE","Import Image...");//Import Image...




$login_check = '<p>To save your images you have to create your account or login if account already exist...<a class="modalCloseImg simplemodal-close" title="Close"></a></p>
							<div style="float:left"><a href="'.$url.'index.php/customer/account/login">Login</a></div><div style="float:left;margin-left:100px;"><a href="'. $url.'index.php/customer/account/create">Register</a></div><div style="float:left;margin-left:100px;"><a href="'. $url.'" class="simplemodal-close">Continue as Guest</a></div>';
							
$about_text = '<p><b>PrintShop Designer V.1.0<br/>Copyright &copy; 2014 Clixmedia GmbH, www.clixmedia.ch</b><a class="modalCloseImg simplemodal-close" title="Close"></a></p>
							
							<p>This online Designer is a fork from svg-edit open source editor. Special thanks goes to Shweta Chaurasia for programming and Marc MacKay for initial designer work.</p>
							<p><i>Method Draw is Copyright &copy;</i><br/><i>Mark MacKay</i></p>
							

							<p><i>SVG Edit is Copyright (c)</br>
							Narendra Sisodiya</br>
							Pavol Rusnak </br>
							Jeff Schiller </br>
							Vidar Hokstad</br>
							Alexis Deveria</i></p>';

$help1 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help2 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help3 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help4 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help5 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';
$help6 = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s';

?>