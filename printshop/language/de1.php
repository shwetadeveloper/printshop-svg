﻿<?php 
define("FILE","Datei");// File
define("NEWDOCUMENT","Neues Dokument");//New Document
define("LAYERS","Ebenen");//Layers
define("NEWLAYER","Neue Ebene");//New Layer
define("DELETELAYER","Ebene löschen");//Delete Layer
define("MOREOPTIONS","Weitere Optionen");//More Options
define("ABOUTTHISEDITOR","Über diesen Editor...");//About this Editor...
define("OPENSVG","Importieren SVG...");//Open SVG...
define("SAVEIMAGETOSERVER","Vorlage SVG auf den Server schreiben");//Save Image to Server
define("CANVAS","Dokument");//Canvas
define("WIDTH","Breite");//Width
define("HEIGHT","Höhe");//Height
define("COLOR","Farbe");//Color
define("CUSTOM","Eigene");//Custom
define("SIZES","Grösse");//Sizes
define("ORDER","Bestellen");//Order
define("SAVE","Sichern");//Save
define("SAVEORDER","Sichern & Bestellen");//Save & order
define("HELP","Hilfe");//Help
define("DESIGNIDEAS","Design Ideen");//Design Ideas
define("IMAGELIBRARY","Bildgalerie");//Image Library
define("QUICKUPLOAD","Schnell Upload");//Quick Upload
define("IMAGEANDGALLERY","Bilder und Galerie");//Image & Gallery
define("EDIT","Edit");//Edit
define("UNDO","Undo");//Undo
define("REDO","Redo");//Redo
define("CUT","Cut");//Cut
define("COPY","Copy");//Copy
define("PASTE","Paste");//Paste
define("DUPLICATE","Duplicate");//Duplicate
define("DELETE","Delete");//Delete
define("_OBJECT","Object");//Object
define("BRINGTOFRONT","Bring to Front");//Bring to Front
define("BRINGFORWARD","Bring Forward");//Bring Forward
define("SENDBACKWARD","Send Backward");//Send Backward
define("SENDTOBACK","Send to Back");//Send to Back
define("GROUPELEMENTS","Group Elements");//Group Elements
define("UNGROUPELEMENTS","Ungroup Elements");//Ungroup Elements
define("CONVERTTOPATH","Convert to Path");//Convert to Path
define("REORIENTPATH","Reorient path");//Reorient path
define("VIEW","View");//View
define("VIEWRULERS","View Rulers");//View Rulers
define("VIEWWIREFRAME","View Wireframe");//View Wireframe
define("SNAPTOGRID","Snap to Grid");//Snap to Grid
define("SOURCE","Source...");//Source...
define("RECTANGLE","Rectangle");//Rectangle
define("PATH","Path");//Path
define("IMAGE","Image");//Image
define("CIRCLE","Circle");//Circle
define("CENTERX","Center X");//Center X
define("CENTERY","Center Y");//Center Y
define("RADIUS","Radius");//Radius
define("ELLIPSE","Ellipse");//Ellipse
define("RADIUSX","Radius X");//Radius X
define("RADIUSY","Radius Y");//Radius Y
define("LINE","Line");//Line
define("STARTX","Start X");//Start X
define("STARTY","Start Y");//Start Y
define("ENDX","End X");//End X
define("ENDY","End Y");//End Y
define("TEXT","Text");//Text
define("FONT","Font");//Font
define("FONTSIZE","Font Size");//Font Size
define("GROUP","Group");//Group
define("EDITPATH","Edit Path");//Edit Path
define("SEGMENTTYPE","Segment Type");//Segment Type
define("STRAIGHT","Straight");//Straight
define("CURVE","Curve");//Curve
define("ADDNOTE","Add Node");//Add Node
define("DELETENODE","Delete Node");//Delete Node
define("OPENPATH","Open Path");//Open Path
define("ROTATION","Rotation");//Rotation
define("OPACITY","Opacity");//Opacity
define("BLUR","Blur");//Blur
define("ROUNDNESS","Roundness");//Roundness
define("ALIGN","Align");//Align
define("MULTIPLEELEMENTS","Multiple Elements");//Multiple Elements
define("STROKE","Stroke");//Stroke
define("STROKEWIDTH","Stroke Width");//Stroke Width
define("UNGROUP","Ungroup");//Ungroup



?>