var messages = {
    IMAGELIBRARY: "Bildgalerie",
	DESIGNIDEA: "Design-Ideen",//Design Ideas
	SELECTANIMAGELIBRARY: "Wählen Sie ein Bild-Bibliothek",//Select an image library
	SHOWLIBRARYLIST: "Bibliotheksliste anzeigen",//Show library list
	IMPORTSINGLE: "Import Einzel",//Import single
	IMPORTMULTIPLE: "Import mehrerer",//Import multiple
	USERSIMAGELIBRARY: "Benutzer-Bildarchiv",//User\'s Image library
	IANSYMBOLLIBRARY: "IAN Symbol-Bibliotheken",//IAN Symbol Libraries
	FREELIBRARY: "Freie Bibliothek von Illustrationen",//Free library of illustrations
};