<!DOCTYPE html>
<html>
<!-- removed for now, causes problems in Firefox: manifest="svg-editor.manifest" -->
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="chrome=1"/>
<link rel="icon" type="image/png" href="images/logo.png"/>

<!--{if svg_edit_release}
  <link rel="stylesheet" href="css/method-draw.compiled.css" type="text/css"/>
{else}>-->
  <link rel="stylesheet" href="lib/jgraduate/css/jPicker.css" type="text/css"/>
  <link rel="stylesheet" href="lib/jgraduate/css/jgraduate.css" type="text/css"/>
  <link rel="stylesheet" href="css/method-draw.css" type="text/css"/>
  <link rel="stylesheet" href="css/fonts.css" type="text/css"/>
  <link rel="stylesheet" href="css/style.css" type="text/css"/>
  <link type='text/css' href='css/basic.css' rel='stylesheet' media='screen' />
<!--<!{endif}-->
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;"/>
  <meta name="apple-mobile-web-app-capable" content="yes"/>


  <script type="text/javascript" src="lib/jquery.js"></script>

<?php include('secure/secure.php');?>
<script type="text/javascript" src="locale/locale.js"></script>
<!--script type="text/javascript" src="extensions/ext-export-to-pdf.js"></script-->

<title>PrintShop Designer</title>
</head>
<body>
<div id="svg_editor">

<?php 

require_once "../../app/Mage.php";
Mage::app();
$lang = Mage::app()->getStore()->getCode();
$lang = 'de';
?>
<script type="text/javascript" src="language/<?php echo $lang;?>.js"></script>
<?php 
$url = $_REQUEST['url'];
include('language/'.$lang.'.php');
if(isset($_REQUEST['pid'])){
	$pid = $_REQUEST['pid'];
	$obj = Mage::getModel('catalog/product');
	$product = $obj->load($pid);
	$productUrl = $product->getProductUrl(); 
	$productUrl = substr($productUrl, 0, strpos($productUrl, "?"));
}
$canvas_size = Mage::getStoreConfig('svgtemplate/settings/canvassize');
$menu_source = Mage::getStoreConfig('svgtemplate/settings/menusource');
$open_svg = Mage::getStoreConfig('svgtemplate/settings/opensvg');
if(isset($_REQUEST['customerId'])){
	$customer_id = $_REQUEST['customerId'];
}
if(isset($_REQUEST['design'])){
	$design = $_REQUEST['design'];
}
if(isset($_REQUEST['ctm'])){
	$fe = $_REQUEST['ctm'];
}


?>

<script>
	//$(document).ready(function (){
    $.ajax({
      url: '<?php echo $url;?>index.php/svgtemplate/index/getCustomerId',
      dataType: "text",
      success: function(data) {
               // alert(data);
				$('#test').val(data);
				$("body").data("test", data);
				
				if(!data){
					$('#basic-modal-content').modal();
				}
            }
    });
//});

$(document).ready(function (){ 
var element = document.getElementById('tool_about');
element.addEventListener("click",clickAbout(),false);
var chk = '<?php echo $fe?>';
/*$.ajax({
      url: '<?php echo $url;?>adminlogin.php',
      dataType: "text",
      success: function(data) {
              // alert(data);
				if((data == 0) && !chk){ 
					window.location.href = "<?php echo $url;?>index.php/admin";
				}
            }
    });*/
});
/*function getwidth(){
	alert($("#canwas_width").val());
}*/
</script>

<?php 
//Check the template file exist or not
if(isset($pid)){
	if(is_file(TEMPLATE_PATH.$pid.'.svg')){
		$exist = 1;
	} else {
		$exist = 0;
	}
}
else if(isset($customer_id)){
		if(is_file(USER_TEMPLATE_PATH.$customer_id.'/'.$design.'.svg')){
		$exist = 1;
	} else {
		$exist = 0;
	}

}





$model = Mage::getModel('svgtemplate/font');
$font_array = $model->getCollection()->addFieldToSelect('font_name')->addFieldToFilter('status', array('eq' => 1))->getData();
foreach($font_array as $values){
	$font_name = $values['font_name'];?>
	<link href="http://fonts.googleapis.com/css?family=<?php echo $font_name;?>" rel="stylesheet" type="text/css" />
<?php }


?>

<div id="rulers">
	<div id="ruler_corner"></div>
	<div id="ruler_x">
	  <div id="ruler_x_cursor"></div>
		<div>
			<canvas height="15"></canvas>
		</div>
	</div>
	<div id="ruler_y">
	  <div id="ruler_y_cursor"></div>
		<div>
			<canvas width="15"></canvas>
		</div>
	</div>
</div>
<div id="workarea">
<div id="svgcanvas" style="position:relative">

</div>
</div>

<input type="hidden" value="<?php echo $pid;?>" name="product_id" id="product_id" />
<input type="hidden" value="<?php echo $customer_id;?>" name="customer_id" id="customer_id" />
<input type="hidden" value="<?php echo $design;?>" name="design" id="design" />
<input type="hidden" value="<?php echo $url;?>" name="url" id="url" />
<input type="hidden" value="<?php echo $exist;?>" name="exist" id="exist" />
<input type="hidden" value="<?php echo $productUrl;?>" name="product_url" id="product_url" />
<input type="hidden" value="<?php echo $lang;?>" name="lang_select" id="lang_select" />

<input type="hidden" value="" id="test" />
<?php if($pid && $exist){
		$str_font = "";
		$path = '../../media/printshop/svgtemplate/'.$pid.'.svg';
		$xdoc = new DomDocument;
		$xdoc->Load($path);
		$text_count = $xdoc->getElementsByTagName('text')->length;
		$i=0;
		while($i<$text_count){
			$tagName = $xdoc->getElementsByTagName('text')->item($i);
			$attribNode = $tagName->getAttributeNode('font-family');
			$str_font .= $attribNode->value.',';
			$i++;
		}
		$str_font = trim($str_font,',');
		
	} else if($customer_id && $exist){
		$str_font = "";
		$path = '../../media/printshop/svgtemplate/user_template/'.$customer_id.'/'.$design.'.svg';
		$xdoc = new DomDocument;
		$xdoc->Load($path);
		$text_count = $xdoc->getElementsByTagName('text')->length;
		$i=0;
		while($i<$text_count){
			$tagName = $xdoc->getElementsByTagName('text')->item($i);
			$attribNode = $tagName->getAttributeNode('font-family');
			$str_font .= $attribNode->value.',';
			$i++;
		}
		$str_font = trim($str_font,',');
	}
?>
<script>

	function clickAbout(){
		$('#tool_about').click(function (e) { 
			$('#basic-modal-content2').modal();
		});
	}
	function clickHelp(){
			$('#basic-modal-content3').modal();
	}
</script>
<input type="hidden" value="<?php echo $str_font;?>" name="font" id="font" />
<?php if($fe != 1) {?>
<div id="sidepanels">
	<div id="layerpanel">
		<h3 id="layersLabel"><?php echo LAYERS;?></h3>
		<fieldset id="layerbuttons">
			<div id="layer_new" class="layer_button"  title="<?php echo NEWLAYER;?>"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24">
  <rect x="2.42792" y="1.6692" width="18" height="21" id="svg_55" fill="#eaeaea" stroke="#606060"/>
  <circle stroke="none" fill="url(#xsvg_9240)" id="svg_65" r="6.300781" cy="7.529969" cx="17.761984"/>
 <defs>
  <radialGradient id="xsvg_9240" cx="0.5" cy="0.5" r="0.5">
   <stop offset="0.1" stop-color="#ffe500" stop-opacity="1"/>
   <stop offset="1" stop-color="#ffff00" stop-opacity="0"/>
  </radialGradient>
 </defs>
</svg></svg></div>
			<div id="layer_delete" class="layer_button"  title="<?php echo DELETELAYER;?>"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <rect ry="3" rx="3" stroke="#800000" fill="#aa0000" id="svg_37" height="20.29514" width="21.17486" y="1.70304" x="1.42011"/>
  <rect ry="3" rx="3" stroke="#ff5555" fill="#aa0000" id="svg_67" height="18.63022" width="19.61118" y="2.53597" x="2.20258"/>
  <line stroke-width="2" fill="none" stroke="#ffffff" id="svg_68" y2="16.85127" x2="17.00646" y1="6.85127" x1="7.00646"/>
  <line stroke-width="2" id="svg_70" fill="none" stroke="#ffffff" y2="16.85127" x2="7.00646" y1="6.85127" x1="17.00646"/>
 </svg></svg></div>
			<div id="layer_rename" class="layer_button"  title="Rename Layer"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg viewBox="0 0 158 128" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	  <text x="58" y="120" id="svg_1" fill="#000000" stroke="none" font-size="120pt" font-family="sans-serif" text-anchor="middle" fill-opacity="1" stroke-opacity="1" font-weight="bold">A</text>
	  <line x1="136" y1="7" x2="136" y2="121" id="svg_2" stroke="#000000" fill="none" fill-opacity="1" stroke-opacity="1" stroke-width="5"/>
	  <line x1="120" y1="4" x2="152" y2="4" id="svg_3" stroke="#000000" stroke-width="5" fill="none" fill-opacity="1" stroke-opacity="1"/>
	 <line x1="120" y1="124" x2="152" y2="124" stroke="#000000" stroke-width="5" fill="none" fill-opacity="1" stroke-opacity="1" id="svg_4"/>
	</svg></svg></div>
			<div id="layer_up" class="layer_button"  title="Move Layer Up"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
	 <defs>
	  <linearGradient y2="0" x2="0.7" y1="0" x1="0" id="xsvg_74360">
	   <stop stop-opacity="1" stop-color="#afe853" offset="0"/>
	   <stop stop-opacity="1" stop-color="#52a310" offset="1"/>
	  </linearGradient>
	 </defs>
	  <path stroke="#008000" fill="url(#xsvg_74360)" id="svg_33" d="m5.38492,16.77043l7.07692,0l0,-5.23077l4.15385,0l-7.69231,-10.15385l-7.69231,10.15385l4.15385,0l0,5.23077z"/>
	</svg></svg></div>
			<div id="layer_down" class="layer_button"  title="Move Layer Down"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
 <defs>
  <linearGradient y2="0" x2="0.7" y1="0" x1="0" id="xsvg_75370">
   <stop stop-opacity="1" stop-color="#afe853" offset="0"/>
   <stop stop-opacity="1" stop-color="#52a310" offset="1"/>
  </linearGradient>
 </defs>
  <path stroke="#008000" fill="url(#xsvg_75370)" id="svg_33" d="m5.3015,1.69202l6.93483,0l0,5.07323l4.07045,0l-7.53786,9.84803l-7.53786,-9.84803l4.07045,0l0,-5.07323z"/>
 </svg></svg></div>
			<div id="layer_moreopts" class="layer_button"  title="<?php echo MOREOPTIONS;?>"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
	  <path stroke-width="0" id="svg_11" d="m4.5,46.5l52,0l-26,38l-26,-38z" stroke="#000" fill="#000"/>
	  <g id="svg_16">
	   <line stroke-width="10" id="svg_12" y2="27.5" x2="117.5" y1="27.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_13" stroke-width="10" y2="51.5" x2="117.5" y1="51.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_14" stroke-width="10" y2="73.5" x2="117.5" y1="73.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_15" stroke-width="10" y2="97.5" x2="117.5" y1="97.5" x1="59.5" stroke="#000" fill="#000"/>
	  </g>
	</svg></svg></div>
		</fieldset>
		
		<table id="layerlist">
			<tr class="layer">
				<td class="layervis"></td>
				<td class="layername">Layer 1</td>
			</tr>
		</table>
		<span id="selLayerLabel">Move elements to:</span>
		<select id="selLayerNames" title="Move selected elements to a different layer" disabled="disabled">
			<option selected="selected" value="layer1">Layer 1</option>
		</select>
	</div>
	<div id="sidepanel_handle" title="Drag left/right to resize side panel [X]">L a y e r s</div>
</div>
<?php } ?>

<!--div id="sidepanels1">
	<div id="layerpanel1">
		<h3 id="layersLabel1">Pages</h3>
		<fieldset id="layerbuttons1">
			<div id="page_new" class="layer_button"  title="New Page"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24">
  <rect x="2.42792" y="1.6692" width="18" height="21" id="svg_55" fill="#eaeaea" stroke="#606060"/>
  <circle stroke="none" fill="url(#xsvg_9240)" id="svg_65" r="6.300781" cy="7.529969" cx="17.761984"/>
 <defs>
  <radialGradient id="xsvg_9240" cx="0.5" cy="0.5" r="0.5">
   <stop offset="0.1" stop-color="#ffe500" stop-opacity="1"/>
   <stop offset="1" stop-color="#ffff00" stop-opacity="0"/>
  </radialGradient>
 </defs>
</svg></svg></div>
			<div id="layer_delete" class="layer_button"  title="<?php echo DELETELAYER;?>"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <rect ry="3" rx="3" stroke="#800000" fill="#aa0000" id="svg_37" height="20.29514" width="21.17486" y="1.70304" x="1.42011"/>
  <rect ry="3" rx="3" stroke="#ff5555" fill="#aa0000" id="svg_67" height="18.63022" width="19.61118" y="2.53597" x="2.20258"/>
  <line stroke-width="2" fill="none" stroke="#ffffff" id="svg_68" y2="16.85127" x2="17.00646" y1="6.85127" x1="7.00646"/>
  <line stroke-width="2" id="svg_70" fill="none" stroke="#ffffff" y2="16.85127" x2="7.00646" y1="6.85127" x1="17.00646"/>
 </svg></svg></div>
			<div id="layer_rename" class="layer_button"  title="Rename Layer"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg viewBox="0 0 158 128" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	  <text x="58" y="120" id="svg_1" fill="#000000" stroke="none" font-size="120pt" font-family="sans-serif" text-anchor="middle" fill-opacity="1" stroke-opacity="1" font-weight="bold">A</text>
	  <line x1="136" y1="7" x2="136" y2="121" id="svg_2" stroke="#000000" fill="none" fill-opacity="1" stroke-opacity="1" stroke-width="5"/>
	  <line x1="120" y1="4" x2="152" y2="4" id="svg_3" stroke="#000000" stroke-width="5" fill="none" fill-opacity="1" stroke-opacity="1"/>
	 <line x1="120" y1="124" x2="152" y2="124" stroke="#000000" stroke-width="5" fill="none" fill-opacity="1" stroke-opacity="1" id="svg_4"/>
	</svg></svg></div>
			<div id="layer_up" class="layer_button"  title="Move Layer Up"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
	 <defs>
	  <linearGradient y2="0" x2="0.7" y1="0" x1="0" id="xsvg_74360">
	   <stop stop-opacity="1" stop-color="#afe853" offset="0"/>
	   <stop stop-opacity="1" stop-color="#52a310" offset="1"/>
	  </linearGradient>
	 </defs>
	  <path stroke="#008000" fill="url(#xsvg_74360)" id="svg_33" d="m5.38492,16.77043l7.07692,0l0,-5.23077l4.15385,0l-7.69231,-10.15385l-7.69231,10.15385l4.15385,0l0,5.23077z"/>
	</svg></svg></div>
			<div id="layer_down" class="layer_button"  title="Move Layer Down"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
 <defs>
  <linearGradient y2="0" x2="0.7" y1="0" x1="0" id="xsvg_75370">
   <stop stop-opacity="1" stop-color="#afe853" offset="0"/>
   <stop stop-opacity="1" stop-color="#52a310" offset="1"/>
  </linearGradient>
 </defs>
  <path stroke="#008000" fill="url(#xsvg_75370)" id="svg_33" d="m5.3015,1.69202l6.93483,0l0,5.07323l4.07045,0l-7.53786,9.84803l-7.53786,-9.84803l4.07045,0l0,-5.07323z"/>
 </svg></svg></div>
			<div id="layer_moreopts" class="layer_button"  title="<?php echo MOREOPTIONS;?>"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="14" height="14" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg_icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
	  <path stroke-width="0" id="svg_11" d="m4.5,46.5l52,0l-26,38l-26,-38z" stroke="#000" fill="#000"/>
	  <g id="svg_16">
	   <line stroke-width="10" id="svg_12" y2="27.5" x2="117.5" y1="27.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_13" stroke-width="10" y2="51.5" x2="117.5" y1="51.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_14" stroke-width="10" y2="73.5" x2="117.5" y1="73.5" x1="59.5" stroke="#000" fill="#000"/>
	   <line id="svg_15" stroke-width="10" y2="97.5" x2="117.5" y1="97.5" x1="59.5" stroke="#000" fill="#000"/>
	  </g>
	</svg></svg></div>
		</fieldset>
		
		<table id="layerlist">
			<tr class="layer">
				<td class="layervis"></td>
				<td class="layername">Layer 1</td>
			</tr>
		</table>
		<span id="selLayerLabel">Move elements to:</span>
		<select id="selLayerNames" title="Move selected elements to a different layer" disabled="disabled">
			<option selected="selected" value="layer1">Layer 1</option>
		</select>
	</div>
	<div id="sidepanel_handle1" title="Drag left/right to resize side panel [X]">P a g e s</div>
</div-->
<!---pages--->
<div id="menu_bar">
  <a class="menu">
    <div class="menu_title" id="logo1"><img src="img/basic/logo.png" height="15" width="15" /></div>
    <div class="menu_list">
      <div id="tool_about" class="menu_item" onclick="clickAbout();"><?php echo ABOUTTHISEDITOR;?></div>
      <!--<div class="separator"></div>
      <div id="tool_about_short" class="menu_item">Keyboard Shortcus...</div>-->
    </div>
  </a>
  
  <div class="menu">
  	<div class="menu_title"><?php echo FILE;?></div>
  	<div class="menu_list" id="file_menu"> 
	<?php if($fe == 1){?>
  		<div id="tool_clear" class="menu_item" style="display:none"><?php echo NEWDOCUMENT; ?></div>
  		<?php if($open_svg == 1) { ?><div id="tool_open" class="menu_item" style="display: none;"><div id="fileinputs"></div><?php echo OPENSVG;?></div><?php } else {?>
			<div id="tool_open" class="menu_item" style="display: none;"></div>
		<?php } ?>
  		<div id="tool_import" class="menu_item" style="display: none;"><div id="fileinputs_import"></div><?php echo IMPORTIMAGE;?></div>
  		<div id="tool_export" class="menu_item" style="display:none">Export as PNG</div>
		<div id="tool_save_image" class="menu_item" style="display:none"><?php echo SAVEIMAGETOSERVER;?></div>
	<?php } else { ?>
		<div id="tool_clear" class="menu_item"><?php echo NEWDOCUMENT; ?></div>
  		<div id="tool_open" class="menu_item" style="display: none;"><div id="fileinputs"></div><?php echo OPENSVG;?></div>
  		<div id="tool_import" class="menu_item" style="display: none;"><div id="fileinputs_import"></div><?php echo IMPORTIMAGE;?></div>
  		<div id="tool_save" class="menu_item">Save Image... <span class="shortcut">⌘S</span></div>
  		<div id="tool_export" class="menu_item">Export as PNG</div>
		<div id="tool_save_image" class="menu_item"><?php echo SAVEIMAGETOSERVER;?></div>
	<?php } ?>
  	</div>
  </div>

  <div class="menu">
    <div class="menu_title"><?php echo EDIT;?></div>
  	<div class="menu_list" id="edit_menu">
  		<div class="menu_item" id="tool_undo"><?php echo UNDO?> <span class="shortcut">⌘Z</span></div>
  		<div class="menu_item" id="tool_redo"><?php echo REDO?> <span class="shortcut">⌘Y</span></div>
  		<div class="separator"></div>
  		<div class="menu_item action_selected disabled" id="tool_cut"><?php echo CUT;?> <span class="shortcut">⌘X</span></div>
  		<div class="menu_item action_selected disabled" id="tool_copy"><?php echo COPY;?> <span class="shortcut">⌘C</span></div>
			<div class="menu_item action_selected disabled" id="tool_paste"><?php echo PASTE?> <span class="shortcut">⌘V</span></div>
  		<div class="menu_item action_selected disabled" id="tool_clone"><?php echo DUPLICATE?> <span class="shortcut">⌘D</span></div>
			<div class="menu_item action_selected disabled" id="tool_delete"><?php echo DELETE;?> <span>⌫</span></div>
    </div>
  </div>
  
  <div class="menu">
    <div class="menu_title"><?php echo _OBJECT;?></div>
  	<div class="menu_list"  id="object_menu">
			<div class="menu_item action_selected disabled" id="tool_move_top"><?php echo BRINGTOFRONT;?> <span class="shortcut">⌘⇧↑</span></div>
			<div class="menu_item action_selected disabled" id="tool_move_up"><?php echo BRINGFORWARD;?> <span class="shortcut">⌘↑</span></div>
			<div class="menu_item action_selected disabled" id="tool_move_down"><?php echo SENDBACKWARD;?> <span class="shortcut">⌘↓</span></div>
			<div class="menu_item action_selected disabled" id="tool_move_bottom"><?php echo SENDTOBACK;?> <span class="shortcut">⌘⇧↓</span></div>
			<div class="separator"></div>
			<div class="menu_item action_multi_selected disabled" id="tool_group"><?php echo GROUPELEMENTS;?> <span class="shortcut">⌘G</span></div>
			<div class="menu_item action_group_selected disabled" id="tool_ungroup"><?php echo UNGROUPELEMENTS;?> <span class="shortcut">⌘⇧G</span></div>
			<div class="separator"></div>
			<div class="menu_item action_path_convert_selected disabled" id="tool_topath"><?php echo CONVERTTOPATH;?></div>
			<div class="menu_item action_path_selected disabled" id="tool_reorient"><?php echo REORIENTPATH;?></div>
    </div>
  </div>

  <div class="menu">
    <div class="menu_title"><?php echo VIEW;?></div>
  	<div class="menu_list" id="view_menu">
  	    <div class="menu_item push_button_pressed" id="tool_rulers"><?php echo VIEWRULERS;?></div>
  	    <div class="menu_item" id="tool_wireframe"><?php echo VIEWWIREFRAME;?></div>
  	    <div class="menu_item" id="tool_snap"><?php echo SNAPTOGRID;?></div>
  	   <?php if($fe != 1 || ($fe == 1 && $menu_source == 1)) {?> <div class="separator"></div>
    		<div class="menu_item" id="tool_source"><?php echo SOURCE;?> <span class="shortcut">⌘U</span></div><?php } ?>
    </div>
  </div>
  
</div>
<div id="svg_docprops"> </div>
<div id="tools_top" class="tools_panel">

	<div id="canvas_panel" class="context_panel">
	  
	  <h4 class="clearfix"><?php echo CANVAS;?></h4>
	  
		<?php if($fe != 1 || ( $fe==1 && $canvas_size == 1)){ ?>
		<label data-title="Change canvas width" >
			<input size="3" id="canvas_width" type="text" pattern="[0-9]*" />
			<span class="icon_label"><?php echo WIDTH;?></span>
		</label>
		<label data-title="Change canvas height">
			<input id="canvas_height" size="3" type="text" pattern="[0-9]*" />
			<span class="icon_label"><?php echo HEIGHT;?></span>
		</label>
		<?php  } else {?>
			<div data-title="Change canvas width" id="div_width">
				<input size="3" id="canvas_width1" type="text" pattern="[0-9]*" data-cursor="false" readonly />
				<span class="icon_label"><?php echo WIDTH;?></span>
			</div>
			<div data-title="Change canvas height" id="div_height">
				<input id="canvas_height1" size="3" type="text" readonly data-cursor="false" />
				<span class="icon_label"><?php echo HEIGHT;?></span>
			</div>
		<?php } ?>
		  	
  	
  	<label data-title="Change canvas color" class="draginput">
    	<span><?php echo COLOR;?></span>
  		<div id="color_canvas_tools">
  		  <div class="color_tool active" id="tool_canvas">
    		  <div class="color_block">
    			  <div id="canvas_bg"></div>
    			  <div id="canvas_color"></div>
    		  </div>
    		</div>
    	</div>
  	</label>

	  <div class="draginput">
	    <span><?php echo SIZES;?></span>
		
		<?php if($fe != 1){?>
  		<select id="resolution">
  			<option id="selectedPredefined" selected="selected"><?php echo CUSTOM;?></option>
  			<!--<option>640x480</option>
  			<option>800x600</option>
  			<option>1024x768</option>
  			<option>1280x960</option>
  			<option>1600x1200</option>-->
			<option value="1189x841">A0</option>
			<option value="841x594">A1</option>
			<option value="594x420">A2</option>
			<option value="420x297">A3</option>
			<option value="297x210">A4</option>
			<option value="210x148">A5</option>
			<option value="148x105">A6</option> 
  			<option id="fitToContent" value="content">Fit to Content</option>
  		</select>
		<?php } ?>
  		<div class="caret"></div>
  		<label id="resolution_label">Custom</label>
		</div>
		
	</div>

	<div id="rect_panel" class="context_panel">
	  <h4 class="clearfix"><?php echo RECTANGLE;?></h4>
	  <label>
			<input id="rect_x" class="attr_changer" data-title="Change X coordinate" size="3" data-attr="x" pattern="[0-9]*" />
			<span>X</span> 
		</label>
		<label>
		  <input id="rect_y" class="attr_changer" data-title="Change Y coordinate" size="3" data-attr="y" pattern="[0-9]*" />
			<span>Y</span> 
		</label>
		<label id="rect_width_tool attr_changer" data-title="Change rectangle width">
			<input id="rect_width" class="attr_changer" size="3" data-attr="width" type="text" pattern="[0-9]*" />
			<span class="icon_label"><?php echo WIDTH;?></span>
		</label>
		<label id="rect_height_tool" data-title="Change rectangle height">
			<input id="rect_height" class="attr_changer" size="3" data-attr="height" type="text" pattern="[0-9]*" />
			<span class="icon_label"><?php echo HEIGHT;?></span>
		</label>
	</div>
	
	<div id="path_panel" class="context_panel clearfix">
	  <h4 class="clearfix"><?php echo PATH;?></h4>
	  <label>
		  <input id="path_x" class="attr_changer" data-title="Change ellipse's cx coordinate" size="3" data-attr="x" pattern="[0-9]*" />
		  <span>X</span>
		</label>
		<label>
		  <input id="path_y" class="attr_changer" data-title="Change ellipse's cy coordinate" size="3" data-attr="y" pattern="[0-9]*" />
		  <span>Y</span>
		</label>
	</div>

	<div id="image_panel" class="context_panel clearfix">
	<h4><?php echo IMAGE;?></h4>
		<label>
			<input id="image_x" class="attr_changer" data-title="Change X coordinate" size="3" data-attr="x"  pattern="[0-9]*"/>
		  <span>X</span> 
		</label>
		<label>
		  <input id="image_y" class="attr_changer" data-title="Change Y coordinate" size="3" data-attr="y"  pattern="[0-9]*"/>
		  <span>Y</span> 
		</label>
		<label>
		  <input id="image_width" class="attr_changer" data-title="Change image width" size="3" data-attr="width" pattern="[0-9]*" />
		  <span class="icon_label"><?php echo WIDTH;?></span>
		</label>
		<label>
		  <input id="image_height" class="attr_changer" data-title="Change image height" size="3" data-attr="height" pattern="[0-9]*" />
		  <span class="icon_label"><?php echo HEIGHT;?></span>
		</label>
  </div>
  
  <div id="circle_panel" class="context_panel">
    <h4><?php echo CIRCLE;?></h4>
		<label id="tool_circle_cx">
		  <span><?php echo CENTERX;?></span>
		  <input id="circle_cx" class="attr_changer" title="Change circle's cx coordinate" size="3" data-attr="cx"/>
		</label>
		<label id="tool_circle_cy">
		  <span><?php echo CENTERY;?></span>
		  <input id="circle_cy" class="attr_changer" title="Change circle's cy coordinate" size="3" data-attr="cy"/>
		</label>
		<label id="tool_circle_r">
		  <span><?php echo RADIUS;?></span>
		  <input id="circle_r" class="attr_changer" title="Change circle's radius" size="3" data-attr="r"/>
		</label>
	</div>

	<div id="ellipse_panel" class="context_panel clearfix">
	  <h4><?php echo ELLIPSE;?></h4>
		<label id="tool_ellipse_cx">
		  <input id="ellipse_cx" class="attr_changer" data-title="Change ellipse's cx coordinate" size="3" data-attr="cx" pattern="[0-9]*" />
		  <span>X</span>
		</label>
		<label id="tool_ellipse_cy">
		  <input id="ellipse_cy" class="attr_changer" data-title="Change ellipse's cy coordinate" size="3" data-attr="cy" pattern="[0-9]*" />
		  <span>Y</span>
		</label>
		<label id="tool_ellipse_rx">
		  <input id="ellipse_rx" class="attr_changer" data-title="Change ellipse's x radius" size="3" data-attr="rx" pattern="[0-9]*" />
	    <span><?php echo RADIUSX;?></span>
		</label>
		<label id="tool_ellipse_ry">
		  <input id="ellipse_ry" class="attr_changer" data-title="Change ellipse's y radius" size="3" data-attr="ry" pattern="[0-9]*" />
		  <span><?php echo RADIUSY;?></span>
		</label>
	</div>

	<div id="line_panel" class="context_panel clearfix">
	  <h4><?php echo LINE;?></h4>
		<label id="tool_line_x1">
		  <input id="line_x1" class="attr_changer" data-title="Change line's starting x coordinate" size="3" data-attr="x1" pattern="[0-9]*" />
		  <span><?php echo STARTX;?></span>
		</label>
		<label id="tool_line_y1">
		  <input id="line_y1" class="attr_changer" data-title="Change line's starting y coordinate" size="3" data-attr="y1" pattern="[0-9]*" />
		  <span><?php echo STARTY;?></span>
		</label>
		<label id="tool_line_x2">
		  <input id="line_x2" class="attr_changer" data-title="Change line's ending x coordinate" size="3" data-attr="x2"   pattern="[0-9]*" />
		  <span><?php echo ENDX;?></span>
		</label>
		<label id="tool_line_y2">
		  <input id="line_y2" class="attr_changer" data-title="Change line's ending y coordinate" size="3" data-attr="y2"   pattern="[0-9]*" />
		  <span><?php echo ENDY;?></span>
		</label>
	</div>

	<div id="text_panel" class="context_panel">
	  <h4><?php echo TEXT;?></h4>
		<label>
		  <input id="text_x" class="attr_changer" data-title="Change text x coordinate" size="3" data-attr="x" pattern="[0-9]*" />
		  <span>X</span>
		</label>
		<label>
		  <input id="text_y" class="attr_changer" data-title="Change text y coordinate" size="3" data-attr="y" pattern="[0-9]*" />
		  <span>Y</span>
		</label>
		
		<div class="toolset draginput select twocol" id="tool_font_family">
				<!-- Font family -->
			<span><?php echo FONT;?></span>
		  <div id="preview_font" style="font-family: Helvetica, Arial, sans-serif;">Helvetica</div>
		  <div class="caret"></div>
			<input id="font_family" data-title="Change Font Family" size="12" type="hidden" />
			<select id="font_family_dropdown">
				  <!--<option value="Helvetica, Arial, sans-serif" selected="selected">Helvetica</option>
					<option value="Arvo, sans-serif">Arvo</option>
					<option value="Euphoria, sans-serif">Euphoria</option>
					<option value="Oswald, sans-serif">Oswald</option>
					<option value="'Shadows Into Light', serif">Shadows Into Light</option>
					<option value="'Simonetta', serif">Simonetta</option>
					<option value="'Trebuchet MS', Gadget, sans-serif">Trebuchet</option>
					<option value="Georgia, Times, 'Times New Roman', serif">Georgia</option>
					<option value="'Palatino Linotype', 'Book Antiqua', Palatino, serif">Palatino</option>
					<option value="'Times New Roman', Times, serif">Times</option>
					<option value="'Courier New', Courier, monospace">Courier</option>
					<option value="Abel">Abel</option>
					<option value="Inconsolata">Inconsolata</option>-->
					<?php foreach($font_array as $values){ ?>
						<option value="<?php echo $values['font_name'];?>"><?php echo $values['font_name'];?></option>
					<?php }?>
			</select>
      <div class="tool_button" id="tool_bold" data-title="Bold Text [B]">B</div>
	  
      <div class="tool_button" id="tool_italic" data-title="Italic Text [I]">i</div>
		</div>

		<label id="tool_font_size" data-title="Change Font Size">
			<input id="font_size" size="3" value="0" />
			<span id="font_sizeLabel" class="icon_label"><?php echo FONTSIZE;?></span>
		</label>
		<!-- Not visible, but still used -->
		<!--input id="text" type="text" size="35"/-->
		<textarea id="text"> </textarea>
	</div>

	<!-- formerly gsvg_panel -->
	<div id="container_panel" class="context_panel clearfix">
	</div>
	
	<div id="use_panel" class="context_panel clearfix">
		<div class="tool_button clearfix" id="tool_unlink_use" data-title="Break link to reference element (make unique)">Break link reference</div>
	</div>
	
	<div id="g_panel" class="context_panel clearfix">
		<h4>Group</h4>
		<label>
		  <input id="g_x" class="attr_changer" data-title="Change groups's x coordinate" size="3" data-attr="x" pattern="[0-9]*" />
		  <span>X</span>
		</label>
		<label>
		  <input id="g_y" class="attr_changer" data-title="Change groups's y coordinate" size="3" data-attr="y" pattern="[0-9]*" />
		  <span>Y</span>
		</label>
	</div>
	
	<div id="path_node_panel" class="context_panel clearfix">
	  <h4><?php echo EDITPATH;?></h4>

		<label id="tool_node_x">
			<input id="path_node_x" class="attr_changer" data-title="Change node's x coordinate" size="3" data-attr="x" />
		  <span>X</span>
		</label>
		<label id="tool_node_y">
			<input id="path_node_y" class="attr_changer" data-title="Change node's y coordinate" size="3" data-attr="y" />
		  <span>Y</span>
		</label>
		
		<div id="segment_type" class="draginput label">
		  <span><?php echo SEGMENTTYPE;?></span>
  		<select id="seg_type" data-title="Change Segment type">
  			<option id="straight_segments" selected="selected" value="4"><?php echo STRAIGHT;?></option>
  			<option id="curve_segments" value="6"><?php echo CURVE;?></option>
  		</select>
  		<div class="caret"></div>
  		<label id="seg_type_label"><?php echo STRAIGHT;?></label>
		</div>
		
		<!--
		<label class="draginput checkbox" data-title="Link Control Points">
		  <span>Linked Control Points</span>
		  <div class="push_bottom"><input type="checkbox" id="tool_node_link" checked="checked" /></div>
		</label>
	-->
		
		<div class="clearfix"></div>
		<div class="tool_button" id="tool_node_clone" title="Adds a node"><?php echo ADDNOTE;?></div>
		<div class="tool_button" id="tool_node_delete" title="Delete Node"><?php echo DELETENODE;?></div>
		<div class="tool_button" id="tool_openclose_path" title="Open/close sub-path"><?php echo OPENPATH;?></div>
		<!--<div class="tool_button" id="tool_add_subpath" title="Add sub-path"></div>-->
	</div>
	
	<!-- Buttons when a single element is selected -->
	<div id="selected_panel" class="context_panel">

		<label id="tool_angle" data-title="Change rotation angle" class="draginput">
			<input id="angle" class="attr_changer" size="2" value="0" data-attr="transform" data-min="-180" data-max="180" type="text"/>
			<span class="icon_label"><?php echo ROTATION;?></span>
			<div id="tool_angle_indicator">
			  <div id="tool_angle_indicator_cursor"></div>
			</div>
		</label>
		
			<label class="toolset" id="tool_opacity" data-title="Change selected item opacity">
  			<input id="group_opacity" class="attr_changer" data-attr="opacity" data-multiplier="0.01" size="3" value="100" step="5" min="0" max="100" />
  		  <span id="group_opacityLabel" class="icon_label"><?php echo OPACITY;?></span>
  		</label>
		
		<div class="toolset" id="tool_blur" data-title="Change gaussian blur value">
			<label>
				<input id="blur" size="2" value="0" step=".1"  min="0" max="10" />
			  <span class="icon_label"><?php echo BLUR;?></span>
			</label>
		</div>
		
		<label id="cornerRadiusLabel" data-title="Change Rectangle Corner Radius">
			<input id="rect_rx" size="3" value="0" data-attr="rx" class="attr_changer" type="text" pattern="[0-9]*" />
		  <span class="icon_label"><?php echo ROUNDNESS;?></span>
		</label>
		
		<div class="clearfix"></div>
		<div id="align_tools">
  		<h4><?php echo ALIGN;?></h4>
  		<div class="toolset align_buttons" id="tool_position">
  				<label>
  				  <div class="col last clear" id="position_opts">
    				  <div class="draginput_cell" id="tool_posleft" title="Align Left"></div>
          		<div class="draginput_cell" id="tool_poscenter" title="Align Center"></div>
          		<div class="draginput_cell" id="tool_posright" title="Align Right"></div>
          		<div class="draginput_cell" id="tool_postop" title="Align Top"></div>
          		<div class="draginput_cell" id="tool_posmiddle" title="Align Middle"></div>
          		<div class="draginput_cell" id="tool_posbottom" title="Align Bottom"></div>
    				</div>
  				</label>
  		</div>		
  	</div>
	</div>
	
	<!-- Buttons when multiple elements are selected -->
	<div id="multiselected_panel" class="context_panel clearfix">
	  <h4 class="hidable"><?php echo MULTIPLEELEMENTS;?></h4>
	  
		<div class="toolset align_buttons" style="position: relative">
		  <label id="tool_align_relative" style="margin-top: 10px;"> 
  			<select id="align_relative_to" title="Align relative to ...">
  			<option id="selected_objects" value="selected">Align to objects</option>
  			<option id="page" value="page">Align to page</option>
  			</select>
  		</label>
		  <h4>.</h4>
    		<div class="col last clear">
      		<div class="draginput_cell" id="tool_alignleft" title="Align Left"></div>
      		<div class="draginput_cell" id="tool_aligncenter" title="Align Center"></div>
      		<div class="draginput_cell" id="tool_alignright" title="Align Right"></div>
      		<div class="draginput_cell" id="tool_aligntop" title="Align Top"></div>
      		<div class="draginput_cell" id="tool_alignmiddle" title="Align Middle"></div>
      		<div class="draginput_cell" id="tool_alignbottom" title="Align Bottom"></div>
    		</div>
		</div>
		<div class="clearfix"></div>

	</div>
	
	<div id="stroke_panel" class="context_panel clearfix">
  	<div class="clearfix"></div>
  	<h4><?php echo STROKE;?></h4>
  	<div class="toolset" data-title="Change stroke">
  		<label>
  			<input id="stroke_width" size="2" value="5" data-attr="stroke-width" min="0" max="99" step="1" />
  		  <span class="icon_label"><?php echo STROKEWIDTH?></span>
  		</label>
  	</div>
  	<div class="stroke_tool draginput"> 
  	  <span>Stroke Dash</span>
  		<select id="stroke_style" data-title="Change stroke dash style">
  			<option selected="selected" value="none">—</option>
  			<option value="2,2">···</option>
  			<option value="5,5">- -</option>
  			<option value="5,2,2,2">-·-</option>
  			<option value="5,2,2,2,2,2">-··-</option>
  		</select>
  		<div class="caret"></div>
  		<label id="stroke_style_label">—</label>
  	</div>
		
    <label style="display: none;">
      <span class="icon_label">Stroke Join</span>
    </label>
    
    <label  style="display: none;">
      <span class="icon_label">Stroke Cap</span>
  	</label>
	</div>
	<!-- Custom options -->
			<div class="image_gallery" id= "image_gallery">
				<div class="image_gallery_txt"><?php echo IMAGEANDGALLERY;?></div>
				<div class="cstm_draginput">
					<!--import image -->
					<div class="quick_upload"><?php echo QUICKUPLOAD;?></div>
					<div class="fileUpload btn btn-primary" id="tool_import1">
						<input type="file" class="upload" />
					</div>
				</div>
			
			
				<!-- Custom options -->
				<?php if($fe == 1){?><div class="cstm_draginput">
										<div class="quick_upload"><?php echo IMAGELIBRARY;?></div>
										<div id="img_gal"><img src="<?php echo $url?>svg/printshop/img/basic/image_gallery.png" /></div>
									 </div>
				<?php }?>
				<div class="cstm_draginput">
					<div class="quick_upload"><?php echo DESIGNIDEAS;?></div>
					<div class="design_idea" ><img src="<?php echo $url?>svg/printshop/img/basic/idea.png" id="design_idea"/></div>
				</div>
				<div class="cstm_draginput">
					<div class="quick_upload"><?php echo HELP;?></div>
					<div class="design_idea" ><img src="<?php echo $url?>svg/printshop/img/basic/help.png" id="help" onclick="clickHelp();"/></div>
				</div>
			</div>
			<div style="position:relative;left:0px;z-index:1;" id="user_guest">
				<div id='container'>
			
					<div id='content'>
						<!--<div id='basic-modal'>
							<input type='button' name='basic' value='Upload' class='basic'/>
						</div>-->
						
						<!-- modal content -->
						<div id="basic-modal-content">
							<?php echo $login_check;?>
						</div>
						
						<div id="basic-modal-content1">
							<p>Name your design..<a class="modalCloseImg simplemodal-close" title="Close"></a></p>
							<div style="float:left">Design Name:<input type="text" value="" id="design_name" style="height:22px;"/></div>
							
							<div><a class="simplemodal-close" title="Submit" onmousedown="javascript:saveDesignName();" id="tool_save_customer_image1"><!--img src="img/basic/submit.png" /--><input type="button" value="Submit" style="height:30px;margin-left:5px;"></a></div>
						</div>
						
						<div id="basic-modal-content2">
							<?php echo $about_text;?>
						</div>
						<!--Help popup-->
						<div id="basic-modal-content3">
							<div style="background-color:#222;height:45px;text-align:center;font-size:15px;padding:1px;"><p><?php echo HELP;?></p></div>
							<div style="overflow:auto;max-height:300px;">
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div class="help_text" style="float:left;width:80%;"><?php echo $help1;?></div>
							</div>
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div style="float:left;width:80%;"><?php echo $help2;?></div>
							</div>
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div style="float:left;width:80%;"><?php echo $help3;?></div>
							</div>
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div style="float:left;width:80%;"><?php echo $help4;?></div>
							</div>
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div style="float:left;width:80%;"><?php echo $help5;?></div>
							</div>
							<div class="fl">
								<div class="help_image"><img src="<?php echo $url?>svg/printshop/img/basic/help.png" alt="" /></div>
								<div style="float:left;width:80%;"><?php echo $help6;?></div>
							</div>
							
							</div>
						</div>
						
						
<script>
function saveDesignName(){
	var design_name = $('#design_name').val();
	if(!design_name){
		alert("Please enter design name");
		return false;
	}
	document.getElementById('design_name_save').value = design_name;
}
</script>
						<!-- preload the images -->
						<div style='display:none'>
							<img src='img/basic/x.png' alt='' />
						</div>
					</div>
					
				</div>
			</div>
			<div class="image_gallery">
				<div class="image_gallery_txt"><?php echo SAVEORDER;?></div>
				<?php if($fe == 1){?>
					<div class="cstm_draginput">
						<div class="quick_upload"><?php echo SAVE;?></div>
						<div class="design_idea"><img src="<?php echo $url?>svg/printshop/img/basic/save.png" id="tool_save_customer_image"/></div>
						<input type="hidden" id="design_name_save" />
						<div style="position:relative;top:-52px;left:20px;display:none;" id="save_loader">
							<img src="<?php echo $url?>svg/printshop/img/basic/ajax-loader.gif">
						</div>
					</div>
				<?php }?>
			
			
			<?php if($fe == 1){?>
				<div class="cstm_draginput">
					<div class="quick_upload"><?php echo ORDER;?></div>
					<div class="design_idea"><img src="<?php echo $url?>svg/printshop/img/basic/addtocart.png" id="add_to_cart"/></div>
					<div style="position:relative;top:-52px;left:20px;display:none;" id="add_cart_loader">
						<img src="<?php echo $url?>svg/printshop/img/basic/ajax-loader.gif">
					</div>
				</div>
			<?php }?>
			</div>
			
</div> <!-- tools_top -->

	<div id="cur_context_panel">
		
	</div>


<div id="tools_left" class="tools_panel">
	<div class="tool_button" id="tool_select" title="Select Tool [V]"></div>
	<div class="tool_button" id="tool_fhpath" title="Pencil Tool [P]"></div>
	<div class="tool_button" id="tool_line" title="Line Tool [L]"></div>
	<div class="tool_button" id="tool_rect" title="Square/Rect Tool [R]"></div>
	<div class="tool_button" id="tool_ellipse" title="Ellipse/Circle Tool [C]"></div>
	<div class="tool_button" id="tool_path" title="Path Tool [P]"></div>
	<div class="tool_button" id="tool_text" title="Text Tool [T]"></div>
	<div class="tool_button" id="tool_zoom" title="Zoom Tool [Z]"></div>
	
  <div id="color_tools">
        <div id="tool_switch" title="Switch stroke and fill colors [X]"></div>
  			<div class="color_tool active" id="tool_fill">
  				<label class="icon_label" title="Change fill color"></label>
				  <div class="color_block">
					  <div id="fill_bg"></div>
					  <div id="fill_color" class="color_block"></div>
				  </div>
  			</div>

  			<div class="color_tool" id="tool_stroke">
  					<label class="icon_label" title="Change stroke color"></label>
  				<div class="color_block">
  					<div id="stroke_bg"></div>
  					<div id="stroke_color" class="color_block" title="Change stroke color"></div>
  				</div>
  			</div>
	</div>
</div> <!-- tools_left -->

<div id="tools_bottom" class="tools_panel">

    <!-- Zoom buttons -->
	<div id="zoom_panel" class="toolset" title="Change zoom level">
		<div class="draginput select" id="zoom_label">
  		<span  id="zoomLabel" class="zoom_tool icon_label"></span>
  		<select id="zoom_select">
  		  <option value="6">6%</option>
  		  <option value="12">12%</option>
  		  <option value="16">16%</option>
  		  <option value="25">25%</option>
  		  <option value="50">50%</option>
  		  <option value="75">75%</option>
  		  <option value="100"  selected="selected">100%</option>
  		  <option value="150">150%</option>
  		  <option value="200">200%</option>
  		  <option value="300">300%</option>
  		  <option value="400">400%</option>
  		  <option value="600">600%</option>
  		  <option value="800">800%</option>
  		  <option value="1600">1600%</option>
  		</select>
  		<div class="caret"></div>
  		<input id="zoom" size="3" value="100%" type="text" readonly="readonly" />
		</div>
	</div>


	
	<div id="tools_bottom_3">
		<div id="palette" title="Click to change fill color, shift-click to change stroke color"></div>
	</div>
</div>

<!-- hidden divs -->
<div id="color_picker"></div>

</div> <!-- svg_editor -->

<div id="svg_source_editor">
	<div id="svg_source_overlay"></div>
	<div id="svg_source_container">
		<div id="save_output_btns">
			<p id="copy_save_note">Copy the contents of this box into a text editor, then save the file with a .svg extension.</p>
			<button id="copy_save_done">Done</button>
		</div>
		<form>
			<textarea id="svg_source_textarea" spellcheck="false"></textarea>
		</form>
		<div id="tool_source_back" class="toolbar_button">
			<button id="tool_source_cancel" class="cancel">Cancel</button>
			<button id="tool_source_save" class="ok">Apply Changes</button>
		</div>
	</div>
</div>
<div id="base_unit_container">
  <select id="base_unit">
  	<option value="px">Pixels</option>
  	<option value="cm">Centimeters</option>
  	<option value="mm">Millimeters</option>
  	<option value="in">Inches</option>
  	<option value="pt">Points</option>
  	<option value="pc">Picas</option>
  	<option value="em">Ems</option>
  	<option value="ex">Exs</option>
  </select>
</div>

<div id="dialog_box">
	<div id="dialog_box_overlay"></div>
	<div id="dialog_container">
		<div id="dialog_content"></div>
		<div id="dialog_buttons"></div>
	</div>
</div>

<ul id="cmenu_canvas" class="contextMenu">
	<li><a href="#cut"><?php echo CUT;?> <span class="shortcut">⌘X;</span></a></li>
	<li><a href="#copy"><?php echo COPY;?><span class="shortcut">⌘C</span></a></li>
	<li><a href="#paste"><?php echo PASTE;?><span class="shortcut">⌘V</span></a></li>
	<li class="separator"><a href="#delete"><?php echo DELETE?><span class="shortcut">⌫</span></a></li>
	<li class="separator"><a href="#group"><?php echo GROUP;?><span class="shortcut">⌘G</span></a></li>
	<li><a href="#ungroup"><?php echo UNGROUP;?><span class="shortcut">⌘⇧G</span></a></li>
  <li class="separator"><a href="#move_front"><?php echo BRINGTOFRONT;?><span class="shortcut">⌘⇧↑</span></a></li>
	<li><a href="#move_up"><?php echo BRINGFORWARD;?><span class="shortcut">⌘↑</span></a></li>
	<li><a href="#move_down"><?php echo SENDBACKWARD;?><span class="shortcut">⌘↓</span></a></li>
  <li><a href="#move_back"><?php echo SENDTOBACK;?><span class="shortcut">⌘⇧↓</span></a></li>
</ul>

<ul id="cmenu_layers" class="contextMenu">
	<li><a href="#dupe">Duplicate Layer...</a></li>
	<li><a href="#delete">Delete Layer</a></li>
	<li><a href="#merge_down">Merge Down</a></li>
	<li><a href="#merge_all">Merge All</a></li>
</ul>
<!--<script type='text/javascript' src='js/jquery.js'></script>-->
<script type='text/javascript' src='js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='js/basic.js'></script>
</body>
</html>

