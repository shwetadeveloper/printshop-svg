<?php

$customer_Id = $_POST['ctmId'];
$output_dir = "../../../media/printshop/svgtemplate/user_image/".$customer_Id.'/';
if (!is_dir($output_dir)) {
    mkdir($output_dir, 0777, true);
}
if(isset($_FILES["myfile"]))
{
	$ret = array();
	$rand = mt_rand(0,9999);
	$error =$_FILES["myfile"]["error"];
	$size =$_FILES["myfile"]["size"];
	list($width, $height) = getimagesize($_FILES['myfile']['tmp_name']);
	//echo $width.'=='.$height;
	if($width > 3000 || $height > 3000){
		$ret[]= 'no';
		echo json_encode($ret);exit;
	} else {
	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData() 
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$fileName = $rand.'_'.$_FILES["myfile"]["name"];
 		move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
    	$ret[]= $fileName;
	}
	else  //Multiple files, file[]
	{
	
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
		$rand = mt_rand(0,9999);
	  	$fileName = $rand.'_'.$_FILES["myfile"]["name"][$i];
		move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
	  	$ret[]= $fileName;
	  }
	
	}
    echo json_encode($ret);
	}
 }
 ?>