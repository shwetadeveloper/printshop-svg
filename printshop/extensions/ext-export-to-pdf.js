/*
 * ext-imagelib.js
 *
 * Licensed under the Apache License, Version 2
 *
 * Copyright(c) 2010 Alexis Deveria
 *
 */

methodDraw.addExtension("pdfexport", function() {
    function exportPDF(){ 
	var url_path = $('#url').val();
        var currentFile = '<?xml version="1.0"?>\n' + svgCanvas.getSvgString();
		
        var xhr = new XMLHttpRequest();
        xhr.open('POST', URL + '/svg2pdf');
        xhr.onload = function(e) {
		//alert(xhr.responseText);
            window.open(URL + xhr.responseText);
		   //window.location = URL + xhr.responseText;
		  // url_new = URL + xhr.responseText;
		  //url_new = 'http://ec2-54-186-48-166.us-west-2.compute.amazonaws.com:8080/svg2pdf/58';
		  /* $.ajax({
				type: "POST",
				url: url_path+"svg/printshop/PDFSave.php?url="+url_new,
				dataType: "text",
				/*data: { 
					svg: str
				},
				success: function(msg) {
					alert(msg);
					
				},
			});*/
        };
        xhr.onerror = function(e) {
            alert('PDF image failed to render');
        };
        var fd = new FormData();
        fd.append("file", currentFile);
        xhr.send(fd);
    }

    var URL = 'http://ec2-54-186-48-166.us-west-2.compute.amazonaws.com:8080';

	var button = $('<div class="menu_item tool_button">Export to PDF</div>');
	button.click(exportPDF);

	//$('.menu_list').append(button);

	return {
		buttons: [{
			id: "tool_exportpdf",
			type: "menu", // _flyout
			position: 7,
			panel: "file_menu",
			title: "Export to PDF",
			events: {
				"click": exportPDF
			}
		}]
	}
});
