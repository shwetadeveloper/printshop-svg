/*
 * ext-multipage.js
 *
 *
 */


methodDraw.addExtension("multipage", function() {
	var parentElement = $('#menu_bar')// TODO change to element where you want buttons added

	var pageButtons = $('<div></div>');

	var pagesArray;
	var currentIndex = 0;
	function openMultipage(svgString) {	
		var svgDom = $('<div>').append(svgString).find('svg');
		if (svgDom.find('page').length) {
			pagesArray = SVGPages.convertSvgPagesToArrays(svgDom[0])
						.map(function(d) {
							return new XMLSerializer().serializeToString(d);
						});
		} else {
			pagesArray = [svgString];
		}
		currentIndex = 0;
		svgCanvas.setSvgString(pagesArray[currentIndex]);
		updateButtons();
	}

	window.openMultipage = openMultipage;

	function saveMultipage() {
		if (pagesArray.length > 1) {
			pagesArray[currentIndex] = svgCanvas.getSvgString();
			return new XMLSerializer().serializeToString(SVGPages.convertSvgArrayToPages(pagesArray
				.map(function(svgString) {
					return $('<div>').append(svgString).find('svg')[0];
				})));			
		} else  {
			return svgCanvas.getSvgString();
		}
	}
	window.saveMultipage = saveMultipage;


	function updateButtons(){
		pageButtons.empty();
		if (pagesArray.length > 1) {
			pagesArray
				.map(function(page, i) {
					return $('<button class="load-page" data-page-index="' + i + '">Page ' + (i + 1) + '</button>');
				})
				.forEach(function(button) {
					button.click(loadPageEvent);
					pageButtons.append(button);
				});
			pagesArray
				.map(function(page, i) {
					return $('<button class="remove-page" data-page-index="' + i + '">Remove Page ' + (i + 1) + '</button>');
				})
				.forEach(function(button) {
					button.click(removePageEvent);
					pageButtons.append(button);
				});		
		}

		updateCurrentPageClass();
	}

	function updateCurrentPageClass() {
		$('.current-page').removeClass('current-page');
		$('[data-page-index="' + currentIndex + '"]').addClass('current-page');		
	}

	function loadPageEvent() {
		loadPage(parseInt(this.getAttribute('data-page-index'), 10));

	}
	function loadPage(newIndex) {
		pagesArray[currentIndex] = svgCanvas.getSvgString();
		currentIndex = newIndex;
		if(!svgCanvas.setSvgString(pagesArray[currentIndex])) {
			console.log('Failed to load page. Removing');
			removePage(currentIndex);			
		}
		updateCurrentPageClass();
	}
	function addPage() {
		startLength = pagesArray.length;
		// firstPage = pagesArray[0];
		// firstPageDom = $('<div>').append(firstPage);
		// firstPageDom.find('g').empty();
		// console.log(new XMLSerializer().serializeToString(firstPageDom[0].firstChild));
		pagesArray.push('<?xml version="1.0"?><svg width="580" height="400" xmlns="http://www.w3.org/2000/svg"> <g>  <title>Layer 1</title> </g></svg>');
		
		loadPage(pagesArray.length);
		updateButtons();
		console.assert(pagesArray.length - startLength === 1);
		console.trace();
	}

	function removePageEvent() {
		removePage(parseInt(this.getAttribute('data-page-index'), 10));
	}

	function removePage(index) {
		startLength = pagesArray.length;
		if (startLength === 1) {
			return
		}
		pagesArray.splice(index, 1);
		if (currentIndex === index) {
			currentIndex -= 1;
			currentIndex = Math.max(currentIndex, 0);
			if(!svgCanvas.setSvgString(pagesArray[currentIndex])) {
				console.log('Failed to load page. Removing');
				removePage(currentIndex);			
			}
			updateCurrentPageClass();
		} else if (currentIndex > index) {
			currentIndex -= 1;
		}

		updateButtons();
		console.assert(startLength - pagesArray.length === 1);
	}


	parentElement.append(pageButtons);
	var addPageButton = $('<button>Add page</button>');
	addPageButton.click(addPage);

	parentElement.append(pageButtons);
	parentElement.append(addPageButton);

	var button = $('<div class="menu_item tool_button">Open multipage</div>');
	button.click(openMultipage);
	$('.menu_list').append(button);

	button = $('<div class="menu_item tool_button">Save multipage</div>');
	button.click(saveMultipage);
	$('.menu_list').append(button);

	mulitpageTests();
	return {
	};
});

function mulitpageTests() {
	$.get('extensions/multipage-test.svg', function(svgString) {
		openMultipage(new XMLSerializer().serializeToString(svgString));// TODO call this when you open a page
	});		
	// $.get('test1.svg', function(svgString) {
	// 	openMultipage(new XMLSerializer().serializeToString(svgString));// TODO call this when you open a page
	// });		
}
