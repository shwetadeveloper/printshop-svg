<!doctype html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

<script>
	
function showDesign(id){
//alert(id);
	 //var selectVal = $('#category :selected').val();
	 $('.des_class').hide();
	 $('.cat_name_div').removeClass('selected');
	 $('#'+id+'_design').show();
	 $('#'+id).addClass('selected');

}
</script>
<style>
	.cat_list_div{
		width:20%;
		float:left;
		color:#fff;
		font: 14px/120% 'Lucida Sans','Lucida Grande','Lucida Sans Unicode',sans-serif;
		background:#000;
		border:1px solid;
		border-radius:5px;
		padding: 5px;
	}
	.cat_name_div{
		height:20px;
		border-radius:5px;
		padding: 5px;
	}
	.cat_name_div:hover{
		background:#333333;
	}
	.selected {
		background: none repeat scroll 0 0 #333333;
	}
	#img_div{
		margin: 0 1%;
		float:left;
		width:75%;
		border:1px solid #fff;
		border-radius:5px;
	}
	.des_class{
		display:none;
		margin-left:10px;
	}
	.cat_image{
		float:left;
		margin:10px;
		width:100px;
	}
	.anchor_text{
		text-decoration:none;
		color:#fff;
		word-wrap: break-word;
	}
</style>
<body>
<!--h1>Select an Idea:</h1-->
<?php   require_once "../../../../app/Mage.php";
// Initialize Magento
Mage::app();
$baseurl = Mage::getBaseUrl();
$category = Mage::getModel('svgtemplate/category')->getCollection()->addFieldToFilter('status',1)->getData();
$design_array = array();
?>
<div style="" class="cat_list_div"> 
<div>
	<?php $j = 0;foreach($category as $val){
	$cat_id = $val['category_id'];
	$cat_name = $val['category_name'];
	$design = Mage::getModel('svgtemplate/design')->getCollection()->addFieldToFilter('category_id',$cat_id)->getData();
	$i=0;
	if($j==0){
			$first_cat = $cat_id;
		}
	$j++;
	
	if(count($design)<=0){
		$design[0]['file_name'] = '0';
	}
	foreach($design as $cat_des){
		$file_name = $cat_des['file_name'];
		
		$design_array[$cat_id][$i] = $file_name;
		$i++;
	}
	?>
	<?php if($cat_id == $first_cat){?>
		<div id="<?php echo $cat_id; ?>" onclick="showDesign(this.id);" class="cat_name_div selected"><?php echo $cat_name;?></div>
	<?php } else {?>
		<div id="<?php echo $cat_id; ?>" onclick="showDesign(this.id);" class="cat_name_div"><?php echo $cat_name;?></div>
<?php } }

?>
</div>
</div>

<!--<select id="category" onchange="showDesign()">
<?php /*foreach($category as $val){
	$cat_id = $val['category_id'];
	$cat_name = $val['category_name'];
	$design = Mage::getModel('svgtemplate/design')->getCollection()->addFieldToFilter('category_id',$cat_id)->getData();
	$i=0;
	foreach($design as $cat_des){
		$file_name = $cat_des['file_name'];
		$design_array[$cat_id][$i] = $file_name;
		$i++;
	}*/
	?>
	<option value="<?php echo $cat_id; ?>"><?php echo $cat_name;?></option>
<?php //}

?>
</select>-->

<?php //$customerId = $_GET['ctid'];?>
<div id="img_div">
<?php  
$dir='../../../../media/printshop/svgtemplate/designs'; $i=1;?>
<?php foreach($design_array as $key => $designs){ ?>
	<?php if($i==1) {?>
		<div id="<?php echo $key.'_design'?>" class="des_class" style="display:block;">
	<?php } else {?>
		<div id="<?php echo $key.'_design'?>" class="des_class">
	<?php } $i++;?>
		<?php if(count($designs)==1 && $designs[0] == 0){ ?>
				<div style="color:#fff;font-size:20px;text-align:center;height:100px;padding-top:50px;">No designs uploaded yet...</div>
			<?php } else { $j = 1;
				foreach($designs as $des_name){ ?>
					<div id="<?php echo $i.'_designs';?>" class="cat_image"><a href="<?php echo $dir.'/'.$des_name; ?>" height="50" width="50" class="anchor_text"/><img src="<?php echo $dir.'/'.$des_name; ?>" height="70" width="70" /><?php echo 'Design '.$j; ?></a><br/></div>
			<?php $j++; }
		}?>
	</div>
<?php } ?>
</div>
</body>

<script>

$('a').click(function test() {
	var href = this.href;
	var target = window.parent;
	// Convert Non-SVG images to data URL first 
	// (this could also have been done server-side by the library)
	if(this.href.indexOf('.svg') === -1) { 

		var meta_str = JSON.stringify({
			name: $(this).text(),
			id: href
		});
		target.postMessage(meta_str, "*");
	
		var img = new Image();
		img.onload = function() {
			var canvas = document.createElement("canvas");
			canvas.width = this.width;
			canvas.height = this.height;
			// load the raster image into the canvas
			canvas.getContext("2d").drawImage(this,0,0);
			// retrieve the data: URL
			try {
				var dataurl = canvas.toDataURL();
			} catch(err) {
				// This fails in Firefox with file:// URLs :(
				alert("Data URL conversion failed: " + err);
				var dataurl = "";
			}
			target.postMessage('|' + href + '|' + dataurl, "*");
		}
		img.src = href;
	} else { 
		// Send metadata (also indicates file is about to be sent)
		var meta_str = JSON.stringify({
			name: $(this).text(),
			id: href
		});
		target.postMessage(meta_str, "*");
		// Do ajax request for image's href value
		$.get(href, function(data) { 
			data = '|' + href + '|' + data;
			// This is where the magic happens!
			target.postMessage(data, "*");
			
		}, 'html'); // 'html' is necessary to keep returned data as a string
	}
	return false;
});

</script>
