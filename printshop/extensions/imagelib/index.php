<!doctype html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>


<body>

<!--h1>Select an image:</h1-->
<?php   require_once "../../../../app/Mage.php";
// Initialize Magento
Mage::app();
$baseurl = str_replace('index.php/','',Mage::getBaseUrl());?>
<script>
	
function deleteImage(cid,img_file,id){
	//alert(id);
	var c = confirm("You want to delete this image?");
	if(c){
		$.ajax({
		  url: '<?php echo $baseurl;?>svg/printshop/extensions/imagelib/userImages.php?img='+img_file+'&cid='+cid,
		  dataType: "text",
		  success: function(data) {
				//alert(data);
				$("#"+id).hide();
				}
		});
	} else {
		
	}
}
</script>
<style>
	#img_div{
		margin: 0 20px;
		float:left;
		width:95%;
		border:1px solid #fff;
		border-radius:5px;
		}
	.cat_image{
		float:left;
		margin:10px;
		width:150px;
	}
	.anchor_text{
		text-decoration:none;
		color:#fff;
		word-wrap: break-word;
	}
</style>
<?php 
if(isset($_GET['ctid'])){
	 $customerId = $_GET['ctid'];
}
if($customerId){
?>
<iframe src="../../multipleupload/index.php?ctmId=<?php echo $customerId;?> " class="input-box" style="width:100%;border:none;overflow: hidden;height:50px;" id="iframe"></iframe>
<div id="result"></div>
<?php  
$dir='../../../../media/printshop/svgtemplate/user_image/'.$customerId.'/';?>
	<div id="img_div">	
		<?php	if (is_dir($dir)) {
				if ($dh = opendir($dir)) {
				$i=0;
					while (($file = readdir($dh)) !== false) {
					if (!in_array($file, array('.', '..')) && !is_dir($dir.$file)) 
                        { $i++; ?>
						<div id="<?php echo $i;?>" class="cat_image"><a href="<?php echo $dir.'/'.$file; ?>" class="anchor_text"/><img src="<?php echo $dir.'/'.$file; ?>" height="100" width="100" alt=""><?php echo "Image_".$i; ?></a>
						<div><img src="../../img/basic/x.png" alt="delete" onclick="deleteImage(<?php echo $customerId; ?>,'<?php echo $file;?>',this.id);" id="<?php echo $i;?>"></div></div>
						
					<?php } //$s = $i++;
					} ?>
				<!--<input type="hidden" value="<?php //echo $s;?>" id="last_img">
				<input type="hidden" value="<?php //echo $dir?>" id="img_dir">-->
				<?php	closedir($dh);
				}
			}?>
	</div>
<?php } ?>
</body>

<script>

$('a').click(function test() { 
	var href = this.href;
	var target = window.parent;
	// Convert Non-SVG images to data URL first 
	// (this could also have been done server-side by the library)
	if(this.href.indexOf('.svg') === -1) { 
		var meta_str = JSON.stringify({
			name: $(this).text(),
			id: href
		});
		target.postMessage(meta_str, "*");
	
		var img = new Image();
		img.onload = function() {
			var canvas = document.createElement("canvas");
			canvas.width = this.width;
			canvas.height = this.height;
			// load the raster image into the canvas
			canvas.getContext("2d").drawImage(this,0,0);
			// retrieve the data: URL
			try {
				var dataurl = canvas.toDataURL();
			} catch(err) {
				// This fails in Firefox with file:// URLs :(
				alert("Data URL conversion failed: " + err);
				var dataurl = "";
			}
			target.postMessage('|' + href + '|' + dataurl, "*");
		}
		img.src = href;
	} else {
		// Send metadata (also indicates file is about to be sent)
		var meta_str = JSON.stringify({
			name: $(this).text(),
			id: href
		});
		target.postMessage(meta_str, "*");
		// Do ajax request for image's href value
		$.get(href, function(data) {
			data = '|' + href + '|' + data;
			// This is where the magic happens!
			target.postMessage(data, "*");
			
		}, 'html'); // 'html' is necessary to keep returned data as a string
	}
	return false;
});

</script>
