from flask import Flask, request, redirect, url_for, send_file, g, Response
import time

import tempfile
import subprocess
import os
import datetime
from flask.ext.cors import cross_origin
from threading import Thread
from werkzeug.exceptions import HTTPException, NotFound
import urllib2


DIRECTORY = 'pdf_directory'
FILE_PREFIX = "pdf_directory/pdf_file"
FILE_POSTFIX = ".pdf"
LOG_FILE = "log.txt"

if not os.path.exists(DIRECTORY):
    os.makedirs(DIRECTORY)


def clearDirectory():
    for dirpath, _, filenames in os.walk(DIRECTORY):
        for file_name in filenames:
            curpath = os.path.join(dirpath, file_name)
            file_modified = datetime.datetime.fromtimestamp(os.path.getmtime(curpath))
            if datetime.datetime.now() - file_modified > datetime.timedelta(days=3):
                os.remove(curpath)

app = Flask(__name__)
app.debug = True

import logging

# logging.basicConfig(filename='log.txt', level=logging.WARNING)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

from logging.handlers import RotatingFileHandler

handler = RotatingFileHandler('logall.txt', mode='a', maxBytes=10000000)
handler.setFormatter(formatter)
handler.setLevel(logging.WARNING)

app.logger.addHandler(handler)


@app.route('/svg2pdf/<file_number>', methods=['GET'])
@cross_origin(headers=['Content-Type']) # Send Access-Control-Allow-Headers
def read_file(file_number):
    outfile = FILE_PREFIX + file_number + FILE_POSTFIX
    try:
        data = open(outfile,'rb').read()
    except Exception, e:
        with open(LOG_FILE, 'a') as log_file:
            log_file.write(time.strftime("%c") + ' ' + str(e) + '\n')
        app.logger.warning(e)
        return NotFound("File not found ")

    thread = Thread(target=clearDirectory)
    thread.start()

    return Response(data, mimetype='application/pdf')


@app.route('/log.txt', methods=['GET'])
def log_file_request():
    returnText = "<p>" + "</p><p>".join(open(LOG_FILE, 'r').readlines()) + "</p>"
    returnText += "<p>" + "</p><p>".join(open('logall.txt', 'r').readlines()) + "</p>"
    returnText += "<p>" + "</p><p>".join(open('all.log', 'r').readlines()) + "</p>"
    return returnText


def create_pdf(svg_text, outfile):
    with tempfile.NamedTemporaryFile(suffix='.svg') as temp_file_obj:
        temp_file_name = temp_file_obj.name

        print 'svg_text len =', len(svg_text)
        with open(temp_file_name, 'w') as tempf:
            tempf.write(svg_text)

        render_command = ' '.join(['phantomjs',
                                   'convert_units.js',
                                   temp_file_name])
        svg_text_no_units = subprocess.check_output(render_command, shell=True)

    with tempfile.NamedTemporaryFile(suffix='.svg') as temp_file_obj:
        temp_file_name = temp_file_obj.name

        with open(temp_file_name, 'w') as tempf:
            tempf.write(svg_text_no_units)

        # render_command = ' '.join(['inkscape', '-f', tempf.name, '-A', outfile])
        render_command = ' '.join(['rsvg-convert', '-f', 'pdf', '-o', outfile, temp_file_name])

        try:
            subprocess.check_output(render_command, shell=True)
        except Exception, e:
            with open(LOG_FILE, 'a') as log_file:
                log_file.write(time.strftime("%c") + ' ' + str(e) +
                               str(request.form['file']) + '\n')

            app.logger.warning(str(e) + str(request.form['file']))

            class RenderErrorRequired(HTTPException):
                code = 400
                description = str(e) + str(request.form['file'])

            return RenderErrorRequired()


def conbine_pdf(file_name_list):
    outfile, file_get_url = get_next_file_name()
    render_command = 'pdfunite ' + ' '.join(file_name_list) + ' ' + outfile
    subprocess.check_output(render_command, shell=True)
    return file_get_url


def convert_multiple(file_text_list):
    file_name_list = []

    # retrieve the name of the temp files
    temporary_files = [tempfile.NamedTemporaryFile()
                       for svg_text in file_text_list]

    for svg_text, temporary_file in zip(file_text_list, temporary_files):
        file_name_list.append(temporary_file.name)

        create_pdf(svg_text, temporary_file.name)
    out_file_url = conbine_pdf(temporary_files)
    for temporary_file in temporary_files:
        temporary_file.close()
    return out_file_url


@app.route('/svg-with-pages2pdf', methods=['POST'])
@cross_origin(headers=['Content-Type']) # Send Access-Control-Allow-Headers
def convert_multiple_request():
    convert_multiple(request.json)


file_count = 0
def get_next_file_name_wrap():
    def get_next_file_name_inner():
        global file_count
        outfile = FILE_PREFIX + str(file_count) + FILE_POSTFIX
        file_get_url = '/svg2pdf/' + str(file_count)

        file_count += 1

        return outfile, file_get_url
    return get_next_file_name_inner
get_next_file_name = get_next_file_name_wrap()


@app.route('/svg2pdf', methods=['POST'])
@cross_origin(headers=['Content-Type']) # Send Access-Control-Allow-Headers
def convert():
    outfile, file_get_url = get_next_file_name()

    if 'file' in request.form:
        svg_text = request.form['file']
    else:
        svg_text = urllib2.urlopen(request.form['url']).read()
        print request.form['url']

    create_pdf(svg_text, outfile)
    return file_get_url

if __name__ == '__main__':
    handler = RotatingFileHandler('all.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run(port=8080, host='0.0.0.0', debug=True)

