var page = require('webpage').create(),
    system = require('system');

page.open(phantom.args[0], function (status) {
    page.injectJs('bower_components/es6-shim/es6-shim.js');
    page.injectJs('svg-pages.js');

    page.onConsoleMessage = function(msg) {
        system.stdout.writeLine(msg);
    };
    var html = page.evaluate(function () {
        var arrayOfPages = window.SVGPages.convertSvgPagesToArrays(document.documentElement, '');

        console.log(JSON.stringify(arrayOfPages.map(function(xml) {return new XMLSerializer().serializeToString(xml);})));
    });

    phantom.exit();
});