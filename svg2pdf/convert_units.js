var page = require('webpage').create(),
    system = require('system');

page.open(phantom.args[0], function (status) {
    page.injectJs('bower_components/es6-shim/es6-shim.js');
    page.injectJs('svg-units.js');

    page.onConsoleMessage = function(msg) {
        system.stdout.writeLine(msg);
    };
    var html = page.evaluate(function () {
        window.SVGUnits.convertSVGToNewUnit(document.documentElement, '');
    });
    
    console.log(page.content);
    phantom.exit();
});