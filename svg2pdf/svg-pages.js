/**
 * Iterates through svg element plus child nodes and converts all length attributes to
 * specified units.
 *
 * Written by Ben McDonald <mcdonald.ben@gmail.com>
 */

/** @type {function():!Object} */
function SVGPagesDefinition() {
    'use strict';


    /**
    * @type {function(!Array.<SVGElement>):SVGElement}
    */
    function convertSvgArrayToPages(arrayOfPages) {
        var newRoot = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        var pageElement = document.createElementNS('http://www.w3.org/2000/svg', 'page');

        arrayOfPages.map(function (e) {
            var pageSetElement = document.createElementNS('http://www.w3.org/2000/svg', 'pageSet');
            pageSetElement.appendChild(e);
            return pageSetElement;
        })
        .forEach(function(pageSetElement) {
            pageElement.appendChild(pageSetElement);
        });

        newRoot.appendChild(pageElement);
        return newRoot;
    }

    /**
    * @type {function(SVGElement):!Array.<SVGElement>}
    */
    function convertSvgPagesToArrays(svgRoot) {
        return Array.from(svgRoot.getElementsByTagName('pageSet')).map(function(pageSet) {
            return pageSet.children[0];
        });
    }

    return {
        'convertSvgArrayToPages': convertSvgArrayToPages,
        'convertSvgPagesToArrays': convertSvgPagesToArrays
    };
}


(function(window) {
    "use strict";
    if ( typeof define === 'function' && define.amd ) {
        // AMD
        define([], SVGPagesDefinition);
    } else {
        window.SVGPages = new SVGPagesDefinition();
    }
})(window);

(function(name, definition) {
    if (typeof module != 'undefined') module.exports = definition();
    else if (typeof define == 'function' && typeof define.amd == 'object') define(definition);
    else this[name] = definition();
}('mod', SVGPagesDefinition));
