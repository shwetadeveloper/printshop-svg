Installation Instructions
=========================

1) Put install.sh at http://yaks.co.nz/install.sh
2) Put svg2pdf.zip at http://yaks.co.nz/svg2pdf.zip -> change url in install.sh
3) Open inbound connects on port 8080

Run on server:
wget -O http://yaks.co.nz/install.sh ; sudo bash install.sh