#!/bin/bash

#wget http://yaks.co.nz/install.sh ; sudo bash install.sh

echo "########### Changing to home dir"
cd ~
echo "########### Updating Ubuntu"
apt-get update -y
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get install python-software-properties
add-apt-repository ppa:chris-lea/node.js
apt-get install python python-pip inkscape unzip librsvg2-bin git iptables-persistent pdftk -y --force-yes
yum install nodejs npm python python-pip inkscape unzip librsvg2-bin git nodejs iptables-persistent pdftk  -y
npm install -g phantomjs bower

echo "########### Setting up autostart (cron)"
crontab -l > tempcron
echo "@reboot python ~/svg2pdf/svg2pdf.py" > tempcron
echo "@reboot sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080" >> tempcron
echo "@reboot git clone https://github.com/w0ng/googlefontdirectory.git;sudo cp -r ~/googlefontdirectory/fonts/* /usr/share/fonts/" >> tempcron
crontab tempcron
rm tempcron

echo "iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080" > /etc/iptables/rules.v4

echo "########### Install Google fonts"
cd ~/
git clone https://github.com/w0ng/googlefontdirectory.git
sudo cp -r ~/googlefontdirectory/fonts/* /usr/share/fonts/truetype/

echo "########### Remove old project"
rm -R ~/svg2pdf
echo "########### Download and install new project"
mkdir ~/svg2pdf
cd ~/svg2pdf
wget http://yaks.co.nz/svg2pdf.zip
unzip svg2pdf
pip install -r requirements.txt
bower install es6-shim

cd ..
chmod -R 777 svg2pdf/

reboot
